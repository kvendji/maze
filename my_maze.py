


def path_check(next_position, maze):
    current_position = next_position.copy()
    possible_steps = []
    for column in range(current_position[1] - 1, current_position[1] + 2):
        for row in range(current_position[0] - 1, current_position[0] + 2):
            if column < 0 or column > len(maze) - 1:
                continue
            elif row < 0 or row > len(maze[column]) - 1:
                continue
            else:
                if maze[row][column] == "." and (row == current_position[0] or column == current_position[1]):
                    if [row, column] not in possible_steps:
                        possible_steps.append([row, column])

    return possible_steps


def look_for_exit(maked_steps, maze, enter):
    all_steps = maked_steps.copy()
    all_steps.remove(enter)
    number_maze = len(maze)-1
    for index in range(0, number_maze):
        if [0, index] in all_steps:
            print(f"Exit is {[0, index]}")
            return ([0,index])
        if [number_maze, index] in all_steps:
            print(f"Exit is {[number_maze, index]}")
            return([number_maze,index])
        if [index, 0] in all_steps:
            print(f"Exit is {[index, 0]}")
            return ([index,0])
        if [index, number_maze] in all_steps:
            print(f"Exit is {[index, number_maze]}")
            return([index,number_maze])


def our_path(maze, enter):
    maked_steps = [enter]
    steps_move = maked_steps

    for step_move in steps_move:
        new_possible_steps = path_check(step_move, maze)

        for new_possible_step in new_possible_steps:
            if new_possible_step not in maked_steps:
                maked_steps.append(new_possible_step)

    return look_for_exit(maked_steps, maze, enter)








