import my_maze


maze1 =["######",
        "...#.#",
        "#.#..#",
        "#....#",
        "####.#",
        "####.#"]


maze2 = ["##########",
         ".........#",
         "######.###",
         "#......###",
         "#.####.###",
         "#........#",
         "##.#######",
         "##.##.####",
         "##.......#",
         "#######.##"]

maze3 = ["####.###############",
         "#....###############",
         "##...........#######",
         "###.#.###.##.#######",
         "#####........#######",
         "####..###.##########",
         "##......######....##",
         "#....#########.##.##",
         "##.#.############.##",
         "#.................##",
         "########..#####..###",
         "###...###.##########",
         "###.#.###.....######",
         "####......####....##",
         "########...#.#######",
         "#...###....#########",
         "###.##.........#####",
         "#...................",
         '######.###.#####.###',
         '####################']







def test_lab():
    maze1_enter = [1,0]
    maze2_enter = [1,0]
    maze3_enter = [0,4]
    maze1_output = [5,4]
    maze2_output = [9,7]
    maze3_output = [17,19]
    for maze, enter, output in [(maze1, maze1_enter, maze1_output),
                                (maze2, maze2_enter, maze2_output),
                                (maze3, maze3_enter, maze3_output)]:
        finder_result = my_maze.our_path(maze,enter)
        print(finder_result, output)
        assert (output == finder_result)
        finder_result = my_maze.our_path(maze, output)
        print(finder_result, enter)
        assert (enter == finder_result)


test_lab()


def test_check_possible_step():
    current_position1 = [3, 3]
    current_position2 = [5, 6]
    current_position3 = [17, 16]
    possible_steps1 = [[3, 2], [2, 3], [3, 3], [3, 4]]
    possible_steps2 = [[5, 5], [4, 6], [5, 6], [5, 7]]
    possible_steps3 = [[17, 15], [17, 16], [18, 16], [17, 17]]
    for maze, current_position, possible_steps in [(maze1, current_position1, possible_steps1),
                                                   (maze2, current_position2, possible_steps2),
                                                   (maze3, current_position3, possible_steps3)]:
        result = my_maze.path_check(current_position, maze)
        print(result, possible_steps)
        assert (possible_steps == result)


test_check_possible_step()